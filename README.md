# README #

### O que é o VITRINI ###

* VITRINI é uma aplicação web que permite a publicação de portifólio inicialmente pensada para o curso de Sistemas e Mídias Digitais da UFC
* Versão 0.1.0(Beta)

### Instalação ###

* A instalação é simples e deve consumir pouco tempo
* O computador/servidor que conterá a aplicação e/ou banco de dados poderá ser simples, mas tenha em mente que o uso do sistema disponível para muitas pessoas requisitará uma máquina potente.

### Recomendamos a seguinte configuração: ###
* CPU: Core 2 Duo 2.3 Ghz ou superior
* RAM: 4GB DDR3
* SO: Ubuntu Linux x64
* HD: 1TB dedicado aos uploads e 500GB para o sistema e banco de dados
* Será necessário: MySql e Apache(ou outro servidor web da sua preferência)
* Para configurar o banco de dados, após instalar o MySql, você precisar criar uma base de dados chamada "vitrini"(sem as aspas) e importar para ela o arquivo "vitrini.sql" presente neste repositório.
* Copie os arquivos correspondentes à aplicação web para o seu servidor web (recomendamos o Apache web Server)
* A melhor forma de testar a aplicação é usando! Tente simular os casos de uso mais gerais para o local onde você deseja usar este sistema

### Participação ###

* Programação:
Hugo Diniz, Alysson Facanha e Leonardo Eversson

* Design:
Karina Paula e Ozéias Dalmon

### Contatos ###

* Administrador do repositório: Hugo Diniz(hhldiniz@gmail.com)