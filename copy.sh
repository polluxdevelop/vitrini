#!/bin/bash

if ! -d "$/opt/lampp" 
	then
		echo "Xampp detectado no seu sistema"
		echo "Copiando dados para a pasta '/opt/lampp/htdocs' "

		cd www/

		sudo cp -r * /opt/lampp/htdocs/
		
		cd ..
		
		sudo cp -r php /opt/lampp/htdocs/


		sudo chmod 755 $(find /opt/lampp/htdocs/ -type d)
		sudo chmod 644 $(find /opt/lampp/htdocs/ -type f)
	else
	if which apache2 
		then
			sudo echo "Copiando diretorios para /var/www/html/"

			cd www/

			sudo cp -r * /var/www/html/
			
			cd ..

			sudo cp -r php/ /var/www/html/


			sudo chmod 755 $(find /var/www/html/ -type d)
			sudo chmod 644 $(find /var/www/html/ -type f)

			
	else
		sudo apt-get install apache2 mysql
clear
fi
fi
echo "Diretorios copiados com sucesso!"