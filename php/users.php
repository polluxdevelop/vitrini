<?php
if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)){


    header('Content-Type:text/html; charset=UTF-8');

    if(!isset($_POST['usuario']) && !isset($_POST['code'])){exit;}
    
    $usuario=$_POST['usuario'];
    $code = $_POST['code'];
    

    $mysqli = new mysqli('localhost', 'root', '', 'pollux');

    mysqli_set_charset($mysqli, 'utf8');

    if($mysqli){

        

        #Listando usuarios para serem seguidos
        function listUsers(){
            global $mysqli;
            global $usuario;

            if(!isset($_POST['usuario2'])){
                $sql = "SELECT usuario FROM usuarios WHERE usuario <> ? AND id NOT IN (SELECT id_usuario2 FROM amizades WHERE id_usuario = (SELECT id FROM usuarios WHERE usuario = ?))";


                $stmt = $mysqli->prepare($sql);
                $stmt->bind_param("ss", $usuario,$usuario);

                if($stmt->execute()){

                	$arr = array();
                	$result = $stmt->get_result();

                    while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                    {
                        $arr[] = $row;
                    }


                    echo json_encode($arr);
                    $stmt->close();
                    $mysqli->close();
                }else{
                    echo 0;

                    $stmt->close();
                    $mysqli->close();
                }

            }
        }

        #Inserindo amizade no banco
        function createFrienship(){
            global $mysqli;
            global $usuario;

                $usuario2 = $_POST['usuario2'];

                $select = "(SELECT id FROM usuarios WHERE usuario = ?)";

                $redundance = $mysqli->prepare("SELECT 1 FROM amizades WHERE id_usuario = ".$select." AND id_usuario2 = ".$select."");
                $redundance->bind_param("ss", $usuario, $usuario2);

                if($redundance->execute() && $redundance->store_result() && $redundance->num_rows<1){

                    $data = date("Y-m-d H:i:s");


                    $sql = "INSERT INTO amizades(id_usuario,id_usuario2,criado) VALUES (";
                        $sql .= $select.",".$select.",?)";


                    $stmt = $mysqli->prepare($sql);

                    $stmt->bind_param("sss" ,$usuario, $usuario2, $data);

                    if($stmt->execute()){

                        echo 1;

                        $stmt->close();
                        $sql = "UPDATE usuarios SET nSeguidores = nSeguidores+1 WHERE id = ".$select;
                        $stmt = $mysqli->prepare($sql);
                        $stmt->bind_param("s",$usuario2);

                        if($stmt->execute()){echo 1;}else{echo 0;}

                        $redundance->close();
                        $stmt->close();
                        $mysqli->close();
                    }else{
                        echo 0;

                        $redundance->close();
                        $stmt->close();
                        $mysqli->close();
                    }
                    }else{
                        echo 0;

                        $redundance->close();
                        $mysqli->close();

                    }
        }


        function listAreas(){
            global $mysqli;

                

                $sql = "SELECT id,nome FROM areas";



                $stmt = $mysqli->prepare($sql);
                #$stmt->bind_param("ss", $usuario,$usuario);

                if($stmt->execute()){
                    $arr = array();
                    $result = $stmt->get_result();

                    while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                    {
                        $arr[] = $row;
                    }

                    echo json_encode($arr);
                    $stmt->close();
                    $mysqli->close();
                }else{
                    echo 0;

                    $stmt->close();
                    $mysqli->close();
                }

        }

        switch ($code) {
            case 1:
                listUsers();
                break;

            case 2:
                createFrienship();
                break;

            case 3:
                listAreas();
                break;

            default:
                exit;
                break;
        }


}else{
    echo 0;
}
}else{
    echo 0;
}


?>