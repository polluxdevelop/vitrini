<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)){

        header('Content-Type:text/html; charset=UTF-8');

        #require_once 'lib/HTMLPurifier/HTMLPurifier.auto.php';

        #$config = HTMLPurifier_Config::createDefault();
        #$purifier = new HTMLPurifier($config);
        
		
		if(!isset($_POST['usuario']) && !isset($_POST['code'])){exit;}

        $usuario=$_POST['usuario'];
        $pass=$_POST['pass'];

        $code = $_POST['code'];

        if(isset($_FILES['capa'])){
            $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
            $detectedType = exif_imagetype($_FILES['capa']['tmp_name']);
        }


        $mysqli = new mysqli('localhost', 'root', '', 'pollux');


        if($mysqli){

            
            
            function convert_array_to_reference($arr){
                $refs = array();
                foreach($arr as $key => $value)
                    $refs[$key] = &$arr[$key];
                return $refs;

            }
                

            # Codigo para listar Posts
            function listPosts($c){
                global $mysqli;
                global $usuario;
                global $pass;

                # Codigo para selecionar posts de um usuario
                if(!isset($_POST['content']) && !isset($_POST["idPost"]) && !isset($_POST['project']) && $c == 1){

                    $select = "(SELECT id FROM usuarios WHERE usuario=? AND senha=?)";

                    $sql = "SELECT posts.id as 'id', posts.nComentarios as 'ncomment', posts.nome as 'titulo', posts.conteudo as 'conteudo',";
                    $sql .= "usuarios.usuario as 'usuario', posts.areas, posts.criado as 'criado', projetos.nome as 'projectname'";
                    $sql .= " FROM posts LEFT JOIN projetos ON posts.id_projeto = projetos.id LEFT JOIN usuarios ON ";
                    $sql .= "posts.id_usuario=usuarios.id WHERE posts.id_usuario =".$select." ORDER BY posts.criado DESC";




                    $stmt = $mysqli->prepare($sql);
                    $stmt->bind_param("ss", $usuario,$pass);

                    if($stmt->execute()){
                        $arr = array();

                        $result = $stmt->get_result();

                        while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                        {
                            $arr[] = $row;
                        }

                        echo json_encode($arr);

                        $stmt->close();
                        $mysqli->close();

                    }else{
                        echo 0;

                        $stmt->close();
                        $mysqli->close();
                    }

                }else if(!isset($_POST['content']) && !isset($_POST["idPost"]) && !isset($_POST['project']) && $c == 6){
                    $select = "(SELECT id_usuario2 FROM amizades WHERE id_usuario=(SELECT id FROM usuarios WHERE usuario=? AND senha=?))";

                    $sql = "SELECT posts.id as 'id', posts.nComentarios as 'ncomment', posts.nome as 'titulo', posts.conteudo as 'conteudo',";
                    $sql .= "usuarios.usuario as 'usuario', posts.areas, posts.criado as 'criado', projetos.nome as 'projectname'";
                    $sql .= " FROM posts LEFT JOIN projetos ON posts.id_projeto = projetos.id LEFT JOIN usuarios ON ";
                    $sql .= "posts.id_usuario=usuarios.id WHERE posts.id_usuario IN".$select." ORDER BY posts.criado DESC";



                    $stmt = $mysqli->prepare($sql);
                    $stmt->bind_param("ss", $usuario,$pass);

                    if($stmt->execute()){
                        $arr = array();

                        $result = $stmt->get_result();

                        while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                        {
                            $arr[] = $row;
                        }

                        echo json_encode($arr);

                        $stmt->close();
                        $mysqli->close();

                    }else{
                        echo 0;

                        $stmt->close();
                        $mysqli->close();
                    }
                }   
            }

            # Codigo para Likes
            function likePost(){
                global $mysqli;
                global $usuario;
                global $pass;

                if(isset($_POST["idPost"]) && !isset($_POST['content']) && !isset($_POST['comment'])){

                    $idPost = $_POST["idPost"];

                    $select = "SELECT id FROM usuarios WHERE usuario=? AND senha=? LIMIT 1";

                        $stmt = $mysqli->prepare($select);
                        $stmt->bind_param("ss", $usuario, $pass);

                        if($stmt->execute()){

                            $result = $stmt->get_result();
                            while ($row = $result->fetch_array(MYSQLI_NUM))
                            {
                                foreach ($row as $r)
                                {
                                    $id = $r;
                                }
                                #print "\n";
                            }

                        $stmt->close();

                        $redundance = $mysqli->prepare("SELECT 1 FROM likes WHERE id_usuario = ? AND id_post = ?");
                        $redundance->bind_param("ii", $id, $idPost);

                        if($redundance->execute() && $redundance->store_result() && $redundance->num_rows<1){

                            $data = date("Y-m-d H:i:s");


                            $sql = "INSERT INTO likes (id_usuario,id_post,criado) values(?,?,?)";

                            $stmt = $mysqli->prepare($sql);
                            $stmt->bind_param("iis", $id, $idPost, $data);

                            if($stmt->execute()){
                                echo 1;

                                $stmt->close();
                                $sql = "UPDATE posts SET nLikes = nLikes+1 WHERE id = ?";
                                $stmt = $mysqli->prepare($sql);
                                $stmt->bind_param("i",$idPost);

                                if($stmt->execute()){echo 1;}else{echo 0;}

                                $stmt->close();
                                $sql = "UPDATE projetos SET nLikes = nLikes+1 WHERE id = (SELECT id_projeto FROM posts WHERE id = ? LIMIT 1)";
                                $stmt = $mysqli->prepare($sql);
                                $stmt->bind_param("i",$idPost);

                                if($stmt->execute()){echo 1;}else{echo 0;}

                                $stmt->close();
                                $sql = "UPDATE usuarios SET nLikes = nLikes+1 WHERE id = (SELECT id_usuario FROM posts WHERE id = ? LIMIT 1)";
                                $stmt = $mysqli->prepare($sql);
                                $stmt->bind_param("i",$idPost);

                                if($stmt->execute()){echo 1;}else{echo 0;}



                                $redundance->close();
                                $stmt->close();
                                $mysqli->close();

                            }else{
                                echo 0;

                                $redundance->close();
                                $stmt->close();
                                $mysqli->close();
                            }

                        }
                    }else{
                        echo 0;

                            $redundance->close();
                            $mysqli->close();
                    }
                }
            }

            # Codigo para Comentarios
            function commentPost(){
                global $mysqli;
                global $usuario;
                global $pass;

                if(isset($_POST["idPost"]) && isset($_POST['comment']) && !isset($_POST['content'])){

                    $comment = $_POST['comment'];
                    $idPost = $_POST["idPost"];

                    $data = date("Y-m-d H:i:s");

                    $select = "(SELECT id FROM usuarios WHERE usuario=? AND senha=?)";

                    $sql = "INSERT INTO comentarios(conteudo,id_post,id_usuario,criado) values(?,?,".$select.",?)";


                    $stmt = $mysqli->prepare($sql);
                    $stmt->bind_param("sisss", $comment, $idPost, $usuario, $pass, $data);

                    if($stmt->execute()){
                        echo 1;

                        $stmt->close();

                        $sql = "UPDATE posts SET nComentarios = nComentarios+1 WHERE id = ? ";
                        $stmt = $mysqli->prepare($sql);
                        $stmt->bind_param("i",$idPost);

                        if($stmt->execute()){

                            echo 1;    
                            $stmt->close();
                            $mysqli->close();
                        }else{
                        
                            echo 0;    
                            $stmt->close();
                            $mysqli->close();
                        }



                    }else{
                        echo 0;

                        $stmt->close();
                        $mysqli->close();
                    }

                }
            }
            
            # Codigo para Postagem
            function createPost(){
                global $mysqli;
                global $usuario;
                global $pass;
                #global $purifier;

                if(isset($_POST['project']) && isset($_POST['content']) && isset($_POST['title'])){
                    $content=$_POST['content'];
                    $project=$_POST['project'];
                    $title=$_POST['title'];

                    $areasarr = $_POST['areas'];

                    $areasarr = json_decode($areasarr);

                    $areas = "";
                    for($i=0;$i<count($areasarr);$i++){
                        $areas .= $areasarr[$i]."/";
                    }

                    #$content = stripslashes($content);
                    #$content = $purifier->purify($content);

                    $select = "SELECT id FROM usuarios WHERE usuario=? AND senha=? LIMIT 1";

                    $stmt = $mysqli->prepare($select);
                    $stmt->bind_param("ss", $usuario, $pass);

                    if($stmt->execute()){

                        $result = $stmt->get_result();
                        while ($row = $result->fetch_array(MYSQLI_NUM))
                        {
                            foreach ($row as $r)
                            {
                                $id = $r;
                            }
                            #print "\n";
                        }

                    $stmt->close();

                    $sql = "INSERT INTO projetos(nome,areas,id_usuario,criado) VALUES(?,?,?,?)";

                    $data = date("Y-m-d H:i:s");

                    $stmt = $mysqli->prepare($sql);
                    $stmt->bind_param("ssis", $project, $areas, $id, $data);

                    if($stmt->execute()){

                        $sql2 = "INSERT INTO posts (nome,conteudo,areas,id_usuario,id_projeto,criado) VALUES";
                        $sql2 .= "(?,?,?,?,?,?)";

                        $project_id = $stmt->insert_id;

                        $stmt->close();

                        echo 1;

                        $data = date("Y-m-d H:i:s");

                        $stmt = $mysqli->prepare($sql2);
                        $stmt->bind_param("sssiis", $title, $content, $areas, $id, $project_id, $data);

                        if($stmt->execute()){

                            echo 1;

                            $stmt->close();

                            $sql = "UPDATE usuarios SET nPosts = nPosts+1, nProjetos = nProjetos+1 WHERE id = ?";
                            $stmt = $mysqli->prepare($sql);
                            
                            #echo $sql;

                            $stmt->bind_param("i",$id);

                            if($stmt->execute()){
                                echo 1;}else{echo 0;}

                            $stmt->close();
                            

                            
                        
                        
                                

                            $sql = "UPDATE projetos SET nPosts = nPosts+1 WHERE id = ?";
                            $stmt = $mysqli->prepare($sql);

                            $stmt->bind_param("i",$project_id);

                            if($stmt->execute()){
                                echo 1;
                            }else{
                                echo 0;
                            }
                            }

                            $stmt->close();
                            $mysqli->close();
                        }else{
                            echo 0;
                            $stmt->close();
                            $mysqli->close();
                        }


                    }else{

                        echo 0;

                        $stmt->close();
                        $mysqli->close();
                    }


                   
                    
                    }else if(isset($_POST['content']) && isset($_POST['title'])){
                        $content=$_POST['content'];
                        $title=$_POST['title'];

                        $areasarr = $_POST['areas'];

                        $areasarr = json_decode($areasarr);

                        $areas = "";
                        for($i=0;$i<count($areasarr);$i++){
                            $areas .= $areasarr[$i]."/";
                        }

                        #echo $content;

                        #$content = $purifier->purify($content);

                        $select = "SELECT id FROM usuarios WHERE usuario=? AND senha=? LIMIT 1";

                        $stmt = $mysqli->prepare($select);
                        $stmt->bind_param("ss", $usuario, $pass);

                        if($stmt->execute()){

                            $result = $stmt->get_result();
                            while ($row = $result->fetch_array(MYSQLI_NUM))
                            {
                                foreach ($row as $r)
                                {
                                    $id = $r;
                                }
                                #print "\n";
                            }

                            $stmt->close();

                            $data = date("Y-m-d H:i:s");

                            $sql = "INSERT INTO posts (nome,conteudo,areas,id_usuario";

                            if(isset($_POST['idProject'])){
                                $idProject = $_POST['idProject'];

                                $sql .= ",id_projeto,criado) VALUES(?,?,?,?,?,?)";
                                $stmt = $mysqli->prepare($sql);

                                #echo $sql;

                                $stmt->bind_param("sssiss", $title, $content, $areas, $id, $idProject, $data);
                            }else{
                                $sql .= ",criado) VALUES (?,?,?,?,?)";
                                $stmt = $mysqli->prepare($sql);

                                #echo $sql;

                                $stmt->bind_param("sssis", $title, $content, $areas, $id, $data);
                            }
                            


                            
                            

                            if($stmt->execute()){
                                echo 1;



                                if(isset($_POST['idProject'])){
                                    $stmt->close();

                                    $sql = "UPDATE projetos SET nPosts = nPosts+1 WHERE id = ?";
                                    $stmt = $mysqli->prepare($sql);

                                    $stmt->bind_param("i",$idProject);
 
                                    if($stmt->execute()){
                                        echo 1;

                                    }else{
                                        echo 0;
                                    }
                                }

                                $stmt->close();

                                $sql = "UPDATE usuarios SET nPosts = nPosts+1 WHERE id = ?";
                                $stmt = $mysqli->prepare($sql);
                                
                                #echo $sql;

                                $stmt->bind_param("i",$id);

                                if($stmt->execute()){
                                    echo 1;}else{echo 0;}

                                #$stmt->close();



                                

                                $stmt->close();
                                $mysqli->close();
                            }else{
                                
                                #echo $sql;
                                #echo ",".$content.",".$usuario.",".$pass.",".$data;
                                #echo $mysqli->error;

                                echo "<div class='alert alert-danger' role='alert'>Erro ao Postar Conteúdo</div>";

                                $stmt->close();
                                $mysqli->close();
                            }
                    }else{
                        echo 0;

                        $stmt->close();
                        $mysqli->close();
                    }


                }else{
                        echo "<div class='alert alert-danger' role='alert'>Erro ao Postar Conteúdo</div>";

                        $stmt->close();
                        $mysqli->close();
                    }
            }

            # Codigo para listagem de projetos
            function listProjects(){
                global $mysqli;
                global $usuario;
                global $pass;

                if(isset($_POST['project']) && !isset($_POST['content'])){
                    $project=$_POST['project'];
                    $project = "%".$project."%";
                    $usuario=$_POST['usuario'];

                    $select = "(SELECT id FROM usuarios WHERE usuario=? AND senha=?)";

                    $sql = "SELECT id,nome FROM projetos WHERE id_usuario = ".$select." AND nome LIKE ?";

                    $stmt = $mysqli->prepare($sql);
                    $stmt->bind_param("sss", $usuario, $pass, $project);

                    if($stmt->execute()){

                        $arr = array();

                        $result = $stmt->get_result();

                        while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                        {
                            $arr[] = $row;
                        }
                        #echo $sql;
                        echo json_encode($arr);

                        $stmt->close();
                        $mysqli->close();


                    }else{
                        echo "<div class='alert alert-danger' role='alert'>Erro ao procurar projetos</div>";

                        $stmt->close();
                        $mysqli->close();
                    }

                
                }
            }


            function createProject(){
                global $mysqli;
                global $usuario;
                global $pass;
                global $detectedType; 
                global $allowedTypes;


                if(isset($_POST['name']) && isset($_POST['descricao'])){

                    $a_params = array();

                    $select = "(SELECT id FROM usuarios WHERE usuario=? AND senha=?)";

                    $sql = "INSERT INTO projetos (nome";
                    $sql2 = "VALUES (?";
                    array_push($a_params, $_POST['name']);


                    if(isset($_FILES['capa']) && $_FILES['capa']['size']<'30000' && $_FILES['capa']['error']<=0 && in_array($detectedType, $allowedTypes)){
                        $sql .= ",capa";
                        $sql2 .= ",?";

                        $usuario = mysqli_real_escape_string($mysqli,$usuario);

                        $usuario = str_replace('..', '', $usuario);
                        $usuario = str_replace('/', '', $usuario);
                        $usuario = str_replace('\\', '', $usuario);

                        $targetPath = "/var/www/html/uploads/".$usuario; // Target path where file is to be stored

                        $ext = pathinfo($_FILES['capa']['name'], PATHINFO_EXTENSION);

                        while (true) {
                            $filename = uniqid(rand(),true);
                            $filename = str_replace('.','',$filename);
                            echo $filename;
                            if (!file_exists($targetPath. "/" . $filename.".".$ext)) break;
                        }

                        $sourcePath = $_FILES['capa']['tmp_name'];       // Storing source path of the file in a variable,
                        $targetPath = $targetPath."/".basename($filename.".".$ext);

                        move_uploaded_file($sourcePath, $targetPath); 

                        $databasePath = "/uploads/".$usuario."/".basename($filename.".".$ext);
                            

                        array_push($a_params,$databasePath);


                    }

                    $sql .= ",descricao,id_usuario,criado) ";
                    $sql2 .= ",?,".$select.",?) ";

                    $data = date("Y-m-d H:i:s");

                    array_push($a_params, $_POST['descricao']);
                    array_push($a_params, $usuario);
                    array_push($a_params, $pass);
                    array_push($a_params, $data);

                    $stmt = $mysqli->prepare($sql.$sql2);

           

                    $types = '';

                    foreach ($a_params as $param) {
                        if(is_int($param)) {
                            $types .= 'i';            
                        } elseif (is_float($param)) {
                            $types .= 'd';             
                        } elseif (is_string($param)) {
                            $types .= 's';              
                        } else {
                            $types .= 'b';              
                        }
                    }

                    array_unshift($a_params, $types);

                    echo $sql.$sql2.", ".json_encode($a_params).", ".$detectedType;

                    call_user_func_array(array($stmt, 'bind_param'), convert_array_to_reference($a_params));

                    if($stmt->execute()){

                        echo 1;

                        $stmt->close();
                        $mysqli->close();
                    }else{
                        echo "<div class='alert alert-danger' role='alert'>Erro ao procurar projetos</div>";

                        $stmt->close();
                        $mysqli->close();
                    }

                
                }
            }


            switch ($code) {
                case 1:
                    listPosts($code);
                    break;
                    
                case 2:
                    likePost();
                    break;

                case 3:
                    commentPost();
                    break;

                case 4:
                    createPost();
                    break;

                case 5:
                    listProjects();
                    break;

                case 6:
                    listPosts($code);
                    break;

                case 7:
                    createProject();
                    break;
                
                default:
                    exit;
                    break;
            }



        }else{
            echo 0;
        }


    }
    else{
        echo 0;
    }

?>