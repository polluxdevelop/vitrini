<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST) || $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES['foto'])){

		/*
		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$usuario = $_POST['usuario'];
		$senha = $_POST['senha'];
		$email = $_POST['email'];
		$telefone = $_POST['telefone'];
		*/

		$usuario = $_POST['usuario'];
		$pass = $_POST['pass'];
		if(isset($_FILES['foto'])){
			$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
			$detectedType = exif_imagetype($_FILES['foto']['tmp_name']);
		}
		$mysqli = new mysqli('localhost', 'root', '', 'pollux');

		if($mysqli){

			function convert_array_to_reference($arr){
			    $refs = array();
			    foreach($arr as $key => $value)
			        $refs[$key] = &$arr[$key];
			    return $refs;

			}	

			$sql = "UPDATE usuarios SET ";

			$count = 0;

			$a_params = array();
 
			if(isset($_POST['nome'])){
				$sql .= "nome = ?";

				array_push($a_params, $_POST['nome']);
				$count++;
			}

			if(isset($_POST['sobrenome'])){
				$sql .= $count > 0 ? "," : "";
				$sql .= "sobrenome = ?";

				array_push($a_params, $_POST['sobrenome']);
				$count++;
			}

			if(isset($_POST['senha'])){
				$sql .= $count > 0 ? "," : "";
				$sql .= "senha = ?";

				array_push($a_params, $_POST['senha']);
				$count++;
			}

			if(isset($_POST['email'])){
				$sql .= $count > 0 ? "," : "";
				$sql .= "email = ?";

				array_push($a_params, $_POST['email']);
				$count++;
			}

			if(isset($_POST['telefone'])){
				$sql .= $count > 0 ? "," : "";
				$sql .= "telefone = ?";

				array_push($a_params, $_POST['telefone']);
				$count++;
			}


			if(isset($_FILES['foto']) && $_FILES['foto']['size']<'30000' && $_FILES['foto']['error']<=0 && in_array($detectedType, $allowedTypes)){

				$sql .= $count > 0 ? "," : "";
				$sql .= "foto = ?";

				$usuario = mysqli_real_escape_string($mysqli,$usuario);

				$usuario = str_replace('..', '', $usuario);
				$usuario = str_replace('/', '', $usuario);
				$usuario = str_replace('\\', '', $usuario);

				$targetPath = "/var/www/html/uploads/".$usuario; // Target path where file is to be stored



				if(! file_exists($targetPath)){
					@mkdir($targetPath,0777,true);
				}

				$ext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);

				while (true) {
	            	$filename = uniqid(rand(),true);
	            	$filename = str_replace('.','',$filename);
	            	echo $filename;
	             	if (!file_exists($targetPath. "/" . $filename.".".$ext)) break;
	            }

	            $sourcePath = $_FILES['foto']['tmp_name'];       // Storing source path of the file in a variable,
	            $targetPath = $targetPath."/".basename($filename.".".$ext);

	            move_uploaded_file($sourcePath, $targetPath); 

	            $databasePath = "/uploads/".$usuario."/".basename($filename.".".$ext);
				
				array_push($a_params, $databasePath);
				$count++;
			}

			if(isset($_POST['areas'])){
				$sql .= $count > 0 ? "," : "";
				$sql .= "areas = ?, profile = ?";

				$areasarr = json_decode($_POST['areas']);

				$areas = "";
				for($i=0;$i<count($areasarr);$i++){
					$areas .= $areasarr[$i]."/";
				}

				array_push($a_params, $areas);
				array_push($a_params, 1);
				$count++;
			}

			if(isset($_POST['interest'])){
				$sql .= $count > 0 ? "," : "";
				$sql .= "inspiracoes = ?";

				$interest = (int)$_POST['interest'];

				$insp = "SELECT inspiracoes FROM usuarios WHERE usuario = ? AND senha = ? LIMIT 1";
				$stmt = $mysqli->prepare($insp);
				$stmt->bind_param("ss",$usuario,$pass);

				if($stmt->execute()){
					$stmt->bind_result($inpira);

					$arrInspira = array();

					while ($stmt->fetch()) {
				        $arrInspira = explode("/", $inpira, -1);
				    }
					$inspira="";
					if(!in_array($interest, $arrInspira)){
						$inspira.= $interest."/".$inspira;

						$update = "UPDATE posts set nInspirados=nInspirados+1 WHERE id=?";
						$stmt = $mysqli->prepare($update);


					}else if(in_array($interest, $arrInspira)){
						for($c=0;$c<count($arrInspira);$c++){
							 if (!$arrInspira[$c] == $interest) {
								$inspira .= $arrInspira[$c]."/";
							}
						}
						$update = "UPDATE posts set nInspirados=nInspirados-1 WHERE id=?";
					}
					$stmt->bind_param("i",$interest);

					if($stmt->execute()){echo 1;}
					}
			

				    $stmt->close();

					array_push($a_params, $inspira);
					$count++;
				}


			

			if($count==0){exit;}

			$sql .= " WHERE usuario = ? AND senha = ?";

			array_push($a_params, $usuario);
			array_push($a_params, $pass);

			echo $sql." , ".$count." , ";

			$stmt = $mysqli->prepare($sql);


			$types = '';

			foreach ($a_params as $param) {
				if(is_int($param)) {
			        $types .= 'i';            
			    } elseif (is_float($param)) {
			        $types .= 'd';             
			    } elseif (is_string($param)) {
			        $types .= 's';              
			    } else {
			        $types .= 'b';              
			    }
			}
			

			array_unshift($a_params, $types);

			call_user_func_array(array($stmt, 'bind_param'), convert_array_to_reference($a_params));

			echo $types.", ".json_encode($a_params);
			


			#$stmt->bind_param("sssssss", , $usuario);
			
			if($stmt->execute()){

				$stmt->close();
				$mysqli->close();

				echo 1;
			}else{

				$stmt->close();
				$mysqli->close();
				
				echo mysqli_error();
				#echo "<div class='alert alert-danger' role='alert'>Um erro ocorreu!</div>";
			}
			

			}else{
				$mysqli->close();
				echo "<div class='alert alert-danger' role='alert'>Esse Nome de Usuário ja esta sendo usado.</div>";
			}
	}else{
		echo 0;
	}
?>