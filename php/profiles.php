<?php
	
	if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)){

		$usuario = $_POST['usuario'];
		$pass = $_POST['pass'];

		header('Content-Type:text/html; charset=UTF-8');


		$mysqli = new mysqli('localhost', 'root', '', 'pollux');

		mysqli_set_charset($mysqli, 'utf8');

		if($mysqli){

			function convert_array_to_reference($arr){
			    $refs = array();
			    foreach($arr as $key => $value)
			        $refs[$key] = &$arr[$key];
			    return $refs;

			}

			
			$stmt = $mysqli->prepare("SELECT nome,sobrenome,areas,inspiracoes,site,ocupacao,telefone,email,foto,nProjetos,nPosts,nSeguidores,nLikes,nVisualizacoes FROM usuarios WHERE usuario = ? AND senha = ? LIMIT 1");
            $stmt->bind_param("ss", $usuario, $pass);

            if($stmt->execute()){

                $arr = array();

                $result = $stmt->get_result();

                while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                {
                    $arr[] = $row;
                }

                $stmt->close();

                $ret = (object)$arr[0];

                if($ret->areas != null){

                	$sql = "SELECT nome FROM areas WHERE id IN ";

                	$a_params = array();

                	$a_params = explode("/", $ret->areas,-1);

                	$inclause = "(";

                		for($i=0;$i<count($a_params);$i++) {
                			if($i==0){
                				$a_params[$i] = (int)$a_params[$i];
                				$inclause .= "?";
                			}else{
                				$inclause .= ",?";
                				$a_params[$i] = (int)$a_params[$i];
                			}

                		}

                	$inclause .= ")";

					$sql .= $inclause;

					$types = '';

					foreach ($a_params as $param) {
						if(is_int($param)) {
							$types .= 'i';            
						} elseif (is_float($param)) {
							$types .= 'd';             
						} elseif (is_string($param)) {
							$types .= 's';              
						} else {
							$types .= 'b';              
						}
					}

					array_unshift($a_params, $types);

					#echo $sql.",".json_encode($a_params);

					$stmt = $mysqli->prepare($sql);

					call_user_func_array(array($stmt, 'bind_param'), convert_array_to_reference($a_params));



					if($stmt->execute()){



						$areas = array();

						$r = $stmt->get_result();

						while($ro = $r->fetch_array(MYSQLI_ASSOC)) 
						{
							$areas[] = $ro;
						}						

						$ret->areas = $areas;


						echo json_encode($ret);
						

						$stmt->close();
						$mysqli->close();

					}else{

						echo $stmt->error;

										#echo 0;
						$stmt->close();
						$mysqli->close();
					}

	        }else{
	        	echo json_encode($ret);
	        	$mysqli->close();
	        }
            }else{
                echo 0;

                $stmt->close();
                $mysqli->close();
            }


	}else{
		echo 0;
	}
	}else{
		echo 0;
	}
	
?>