$(document).ready(function()
{
   $("#alterar_cadastro").validade({
       rules: {
                    nome: {required:false},
                    sobrenome: {required:false},

                    usuario:{
                    	required: false,
                    	minlength: 4
                    },

                    email: {
                        required: false,
                        email: false
                    },
                    senha: {
                        required: false,
                        minlength: 8
                    },

                    senha_confirma: {
                    	required: false,
                    	minlength: 8,
                    	equalTo: "#senha"
                    }



                },
                messages: {
                    sobrenome: {required: "Por favor digite seu sobrenome"},

                    usuario:{
                    	minlength: 'Nome de usuario deve conter no mínimo 4 caracteres'
                    },
                    senha: {
                        minlength: "Sua senha deve ter no mínimo 8 caracteres"
                    },

                    senha_confirma: {
                    	minlength: "Sua senha deve ter no mínimo 8 caracteres",
                    	equalTo: "Senhas não são iguais"
                    },

                    email: "Por favor insira um endereço de email válido"


                },
                submitHandler: function(form) {
                    var values;
                    if($("#nome").val()!="")
                    {
                        values+="nome="+$("#nome").val();
                    }
                    if($("#usuario").val()!="")
                    {
                        values+="&usuario="+$("#usuario").val();
                    }
                    if($("#email").val()!="")
                    {
                        values+="&email="+$("#email").val();
                    }
                    if($("#senha").val()!="")
                    {
                        values+="$senha="+$("#senha").val();
                    }
                    var server = "http://localhost/polluxsocial/php/";
                    var file = "alterarCadastro.php";

                    $("#status").html("Enviando...");
                    $.ajax({
                        type: "POST",
                        url: server+file,
                        datatype: "html",
                        data: values,
                        success: function(data){
                            $("#status").html(data);
                            console.log(data);
                        }

                    });

                    return false;

                }
            });
   });
