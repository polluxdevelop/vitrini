﻿$(document).ready(function()
{


	$("#cadastro").validate({
                rules: {
                    nome: {required:true},
                    sobrenome: {required:true},

                    usuario:{
                    	required: true,
                    	minlength: 4
                    },

                    email: {
                        required: true,
                        email: true
                    },
                    senha: {
                        required: true,
                        minlength: 8
                    },

                    senha_confirma: {
                    	required: true,
                    	minlength: 8,
                    	equalTo: "#senha"
                    }



                },
                messages: {
                    nome: {required: "Por favor digite seu nome"},
                    sobrenome: {required: "Por favor digite seu sobrenome"},

                    usuario:{
                    	required: 'Por favor insira um nome de usuario',
                    	minlength: 'Nome de usuario deve conter no mínimo 4 caracteres'
                    },
                    senha: {
                        required: "Por favor digite uma senha",
                        minlength: "Sua senha deve ter no mínimo 8 caracteres"
                    },

                    senha_confirma: {
                    	required: "Por favor confirme sua senha",
                    	minlength: "Sua senha deve ter no mínimo 8 caracteres",
                    	equalTo: "Senhas não são iguais"
                    },

                    email: "Por favor insira um endereço de email válido"


                },
                submitHandler: function(form) {
                    var nome = $("#nome").val();
                    var sobrenome = $("#sobrenome").val();
                    var usuario = $("#usuario").val();
                    var senha = $("#senha").val();
                    var email = $("#email").val();
                    var tel = $("#tel").val();

                    var values = "nome="+nome+"&sobrenome="+sobrenome+
                    "&usuario="+usuario+"&senha="+senha+"&email="+email+"&telefone="+tel;

                    var server = "http://localhost/polluxsocial/php/";
                    var file = "cadastro.php";

                    $("#status").html("Enviando...");
                    $.ajax({
                        type: "POST",
                        url: server+file,
                        datatype: "html",
                        data: values,
                        success: function(data){
                            $("#status").html(data);
                            console.log(data);
                        }

                    });

                    return false;

                }
            });

});
