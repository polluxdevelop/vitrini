/*
    ESTE SCRIPT FOI CRIADO PARA FORÇAR UMA PROPRIEDADE CSS POIS O BOOTSTRAP IMPEDE MUDANÇAS GRANDES EM SEU LAYOUT
*/

$(document).ready(function(){
   var largura=($(window).width()/100)*35;
    $("#item1").css("margin-left",largura);
    $(window).onchange(function(){
       largura=($(window).width()/100)*35;
        $("#item1").css("margin-left",largura);
    });
});
