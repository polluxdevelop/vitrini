var server = "php/";


function validateSession(){ //loga no sistema automaticamente caso existam dados no localStorage
    if(typeof(localStorage.user) === "undefined" || typeof(localStorage.pass) === "undefined"){
        location.href = "index.html";
        return false;
    }else{
        return true;
    }
}




validateSession();

$.ajax( //salva no localStorage o usuário e senha do usuário em questão para que ele possa voltar sem digitar os dados novamente
{
    type: "POST",
    url: server+"login.php",
    datatype: "html",
    data: "usuario="+localStorage.user+"&senha="+localStorage.pass,
    success: function(data)
    {
        var ret = parseInt(data);
        if(ret < 1){
            location.href = "index.html";

        }else if(ret >= 2){
            location.href = "profile.html";

        }
    }
});
$(document).ready(function(){
    var users = [];
    var areas = [];
    var personalAreas = [];
    var pAreas = [];
    var count = 0;


    
    




    function listUsers(){
        var values = "usuario="+localStorage.user+"&code=1";
        
        $.ajax({
            type: "POST",
            url: server+"users.php",
            datatype: "html",
            data: values,
            success: function(data){

                if(data != "[]"){
                    users = JSON.parse(data);


                    $("#users").html("Siga pessoas batutas: <br />");
                    for(var i=0;i<users.length;i++){
                        $("#users").append("<div class='potencialFriend'>"+users[i].usuario+
                            " <button class='btFollow' action='#'></button></div>");
                    }

                    $("#users").on('click', '.btFollow', function() {
                                    followUser(users[($(this).closest('.potencialFriend').index()-1)].usuario);
                                });

                }
            }

        });
    }

     function followUser(user){
        if(validateSession()){
            var values = "usuario="+localStorage.user+"&usuario2="+user+"&code=2";

            $.ajax({
                type: "POST",
                url: server+"users.php",
                datatype: "html",
                data: values,
                success: function(data){
                    var ret = parseInt(data);
                    if(ret === 1){
                        users.splice(user,1);
                        
                        $("#users").html("Siga pessoas batutas: <br />");
                        for(var i=0;i<users.length;i++){
                            $("#users").append("<div class='potencialFriend'>"+users[i].usuario+
                                " <button class='btFollow' action='#'></button></div>");
                        }
                    }
                    
                }

            });

        }
    }

    if(validateSession()){listUsers();}

    function listAreas(){

        var values = "usuario="+localStorage.user+"&code=3";

        $.ajax({
        type: "POST",
        url: server+"users.php",
        datatype: "html",
        data: values,
        success: function(data){

            areas = JSON.parse(data);

            showAreas();
            
        }});


    }

    if(validateSession()){listAreas();}

    function showAreas(){
        for(i=0;i<areas.length;i++){
                $("#areas").append("<div class='area'>"+areas[i].nome+"</div>");
            }

            $("#areas").on('click', '.area', function() {
                var c = $(this).index()-1;

                pAreas.push(areas[c].id);
                personalAreas.push(areas[c]);

                $("#personal-areas").append("<div class='area'>"+areas[c].nome+"</div>");
                areas.splice(c,1);
                $(this).remove();

                console.log("areas: "+JSON.stringify(areas)+", pAreas: "+JSON.stringify(pAreas)+", count: "+c);
            });

            $("#personal-areas").on('click', '.area', function() {
                var c = $(this).index()-1;

                pAreas.splice(c,1);
                areas.push(personalAreas[c]);
                personalAreas.splice(c,1);

                $("#areas").append("<div class='area'>"+areas[areas.length-1].nome+"</div>");
                $(this).remove();
            });

    }

    
    $("#submit-areas").click(function(){
        if(!pAreas.length<1 && validateSession()){

            var values = "usuario="+localStorage.user+"&pass="+localStorage.pass+"&areas="+JSON.stringify(pAreas);

            $.ajax({
                type: "POST",
                url: server+"update.php",
                datatype: "html",
                data: values,
                success: function(data){
                    var ret = parseInt(data);
                    location.href="/profile.html"
                }

            });

        }else
        {
            noty({
                text: 'Você precisa escolher 3 ou mais áreas',
                layout: 'bottom',
                type: "warning",
                closeWith: ['button']
            });
        }
    });


    function listPersonalArea(pa){
                for(count=0;count<pa.length;count++){
                    $("#personal-areas").append("<div class='area'>"+pa[count]+"</div>");
                }
    }


$("#photo").change(function(){

    var foto = $('#photo').prop('files')[0];

    var form_data = new FormData();                  
    form_data.append('foto', foto);
    form_data.append('usuario', localStorage.user);
    form_data.append('pass', localStorage.pass);

    $.ajax({
        type: "POST",
        url: server+"update.php",
        datatype: "html",
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        success: function(data){


            console.log(data);



                        }

                    });
});





    $("#sair").click(function(){ //faz o logout da aplicação
        localStorage.clear();
        location.href = "index.html";
    });



});
