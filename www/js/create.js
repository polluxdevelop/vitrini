var server = "php/";


function validateSession(){ //loga no sistema automaticamente caso existam dados no localStorage
    if(typeof(localStorage.user) === "undefined" || typeof(localStorage.pass) === "undefined"){
        location.href = "index.html";
        return false;
    }else{
        return true;
    }
}




validateSession();

$.ajax( //salva no localStorage o usuário e senha do usuário em questão para que ele possa voltar sem digitar os dados novamente
{
    type: "POST",
    url: server+"login.php",
    datatype: "html",
    data: "usuario="+localStorage.user+"&senha="+localStorage.pass,
    success: function(data)
    {
        var ret = parseInt(data);
        if(ret < 1){
            location.href = "index.html";

        }else if(ret < 2){
            location.href = "intro.html";

        }
    }
});

$(document).ready(function(){

	var idProject;
    var projects;
    var xhr;

    var pAreas = [];
    var personalAreas = [];
    var areas = [];




    var escapeHtml = (function () {
        'use strict';
        var chr = { '"': "'" };
        return function (text) {
            return text.replace(/[\"]/g, function (a) { return chr[a]; });
        };
    }());

    tinymce.init({ 
        selector:'#content',
        language: 'pt_BR',
        language_url: 'js/lib/tinymce/langs/pt_BR.js',
        plugins: "image imagetools media link autolink paste",
        imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
        file_picker_types: 'image media'
    });



    $("#project").on("keypress",function(){ 
        //faz uma requisição ao banco a cada tecla pressionada dentro do campo de texto com ID project a fim de pesquisar os projetos existentes
        if(xhr && xhr.readyState != 4){
            xhr.abort();
        }

        var dados = "usuario="+localStorage.user+"&pass="+localStorage.pass+"&project="+$("#project").val()+"&code=5";

        var xhr =  $.ajax(
        {
           type: "POST",
           url: server+"posts.php",
           datatype: "html",
           data: dados,
           success: function(data)
           {
            projects = JSON.parse(data);

            for(i=0;i<projects.length;i++){
                $("#status").append("<p id='proTag'>"+projects[i].nome+"</p>");
            }

            $("#status").on('click', '#proTag', function() {
                console.log($(this).index() );
                idProject = projects[($(this).index())].id;
            });

        }
    });

    });



	$("#new-post").validate({
            rules: {
                title: {required:true},
                content: {required: true, minlength: 1}
         },
         messages:
         {
            title: {
                required: "Este campo é obrigatório."
            },
            content: {
                required: "Este campo é obrigatório.",
                minlength: "Descrição precisa ter pelo menos 40 caracteres"
            }
        },
        submitHandler: function(form)
        {
            
            if(pAreas.length>0 && content!=""){
                var title = $("#title").val();

                var content = tinyMCE.activeEditor.getContent();

                content = escapeHtml(content);

                content = content.replace(/\r?\n|\r/g,"");
                content = encodeURIComponent(content);
                console.log(content);

                var dados = "title="+title+"&content="+content+"&usuario="+localStorage.user+"&pass="+localStorage.pass+"&code=4";

                if(idProject === undefined && $("#project").val()!="" || idProject === -1 && $("#project").val()!=""){
                    var project = $("#project").val();
                    dados += "&project="+project;
                }else if(idProject != -1 && idProject != undefined){
                    dados += "&idProject="+idProject;
                }

                
                    var ar = JSON.stringify(pAreas);
                    dados += "&areas="+ar;
                


                    $.ajax(
                    {
                       type: "POST",
                       url: server+"posts.php",
                       datatype: "html",
                       data: dados,
                       success: function(data)
                       {
                           noty({
                                text: "Novo post inserido",
                                type: "success",
                                layout: "topRight",
                                closeWith: ['button']
                           });
                       },
                       error: function(data)
                       {
                            noty({
                                text: "Erro ao tentar inserir o post. Retorno: "+data,
                                type: "error",
                                layout: "topRight",
                                closeWith: ['button']
                            });
                       }
                   });
                
            }else{
                $("#status").html("Insira pelo menos uma area ao seu projeto. :p");
            }
            

            return false;
        }
    });

    
    $("#new-project").validate({
            rules: {
                name: {required:true},
                descricao: {required: true, minlength: 20}
         },
         messages:
         {
            name: {
                required: "Este campo é obrigatório."
            },
            descricao: {
                required: "Este campo é obrigatório.",
                minlength: "Descrição precisa ter pelo menos 20 caracteres"
            }
        },
        submitHandler: function(form)
        {

            var name = $("#name").val();
            var descricao = $("#descricao").val();
            var file = $("#capa").prop('files').length;
            
            var form_data = new FormData();                  

            if(file>0 && file<2){
                var capa = $("#capa").prop('files')[0];
                form_data.append('capa', capa);
            }

            form_data.append('name', name);
            form_data.append('descricao', descricao);
            form_data.append('code', 7);
            form_data.append('usuario', localStorage.user);
            form_data.append('pass', localStorage.pass);

            $.ajax({
                type: "POST",
                url: server+"posts.php",
                datatype: "html",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: function(data){
                    noty({
                        text: "Novo projeto inserido",
                        layout: "topRight",
                        type: "success"
                    });
                },
                error: function(data){
                    noty({
                        text: "Um erro ocorreu ao tentar postar este conteúdo. Retorno: "+data,
                        type: "error",
                        layout: "bottom"
                    });
                }

            });

            return false;
        }
    });


    function listAreas(){

        var values = "usuario="+localStorage.user+"&code=3";

        $.ajax({
        type: "POST",
        url: server+"users.php",
        datatype: "html",
        data: values,
        success: function(data){

            areas = JSON.parse(data);


            for(i=0;i<areas.length;i++){
                $("#areas").append("<div class='area'><span>"+areas[i].nome+"</span></div>");
            }

            $("#areas").on('click', '.area', function() {
                var c = $(this).index()-1;

                pAreas.push(areas[c].id);
                personalAreas.push(areas[c]);

                $("#personal-areas").append("<div class='area'>"+areas[c].nome+"</div>");
                areas.splice(c,1);
                $(this).remove();

                console.log("areas: "+JSON.stringify(areas)+", pAreas: "+JSON.stringify(pAreas)+", count: "+c);
            });

            $("#personal-areas").on('click', '.area', function() {
                var c = $(this).index()-1;

                pAreas.splice(c,1);
                areas.push(personalAreas[c]);
                personalAreas.splice(c,1);

                $("#areas").append("<div class='area'>"+areas[areas.length-1].nome+"</div>");
                $(this).remove();
            });
            
        }});


    }

    if(validateSession()){listAreas();}

   function getProfile(){

        $.ajax( //salva no localStorage o usuário e senha do usuário em questão para que ele possa voltar sem digitar os dados novamente
        {
            type: "POST",
            url: server+"profiles.php",
            datatype: "html",
            data: "usuario="+localStorage.user+"&pass="+localStorage.pass,
            success: function(data)
            {

                profile = JSON.parse(data);

                $("#profile-photo").attr("src", profile.foto);

                if(profile.nome[profile.nome.length-1] === " " || profile.sobrenome[0] === "" ){
                    var nome = profile.nome+profile.sobrenome;
                }else{
                    var nome = profile.nome+" "+profile.sobrenome;
                }

                $("#complete-name").html(nome);
                $('#nome_usuario').html(nome);
                
                $("#ocupation").html(profile.ocupacao);

                $("#site").attr("href",profile.site);
                $("#site").html(profile.site);

                //$("#vizualizado a").append(profile.nVisualizacoes);
                $("#comentado a").append(profile.nPosts);
                $("#favorito a").append(profile.nLikes);
                $("#envolvidos a").append(profile.nSeguidores);

                for(i=0;i<profile.areas.length;i++){
                    $("#profile-areas").append("<div class='area'><span>"+profile.areas[i].nome+"</span></div>");
                }
            }
        });

    }



        if(validateSession()){
            getProfile();
        }

});