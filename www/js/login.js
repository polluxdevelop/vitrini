﻿var server = "php/";
var file = "login.php";
function validateSession(){
        if(typeof(localStorage.user) === "undefined" && typeof(localStorage.pass) === "undefined"){
            return false;
        }else{
            return true;
        }
    }

if(validateSession()){
        var values = "usuario="+localStorage.user+"&senha="+localStorage.pass;
                    $.ajax({
                        type: "POST",
                        url: server+file,
                        datatype: "html",
                        data: values,
                        success: function(data){
                            if(parseInt(data) >= 1){
                                location.href="profile.html"
                            }
                            
                            console.log(data);
                        }

                    });
    }

$(document).ready(function()
{
    $('#login').each(function(){
         this.reset();
    })

    $("#login").validate({
                rules: {
                    usuario:{required:true,
                            minlength: 4
                    },

                    senha:{
                        required:true,
                        minlength: 8
                    }
                },
                messages:{
                    usuario: {
                        required: "Por favor insira seu nome de usuário",
                        minlength: 'Nome de usuário deve conter no mínimo 4 caracteres'
                    },
                    senha: {
                        required: "Digite sua senha",
                        minlength: "Sua senha deve ter no mínimo 8 caracteres"
                    }
                },
                submitHandler: function(form) {
                    
                    var usuario = $("#usuario").val();
                    var senha = $("#senha").val();
                    var values = "usuario="+usuario+"&senha="+senha;

                    

                    $("#status").html("Enviando...");
                    $.ajax({
                        type: "POST",
                        url: server+file,
                        datatype: "html",
                        data: values,
                        success: function(data){
                            if(parseInt(data) == 0)
                            {
                                noty({
                                    text: 'Dados incorretos',
                                    layout: 'bottom',
                                    type: 'error',
                                    closeWith: ['button']
                                });
                            }
                            if(parseInt(data) === 1){
                                localStorage.setItem("user", usuario);
                                localStorage.setItem("pass", senha);

                                location.href = "intro.html";
                            }else if(parseInt(data) === 2){
                                localStorage.setItem("user", usuario);
                                localStorage.setItem("pass", senha);

                                location.href = "timeline.html";
                            }
                        },
                        error: function(data)
                        {
                            noty({
                                text: "Um erro ocorreu ao tentar conectar ao banco de dados",
                                type: "error",
                                layout: "bottom"
                            });
                        }
                    });

                    return false;

                }
            });

     $(".btn-registro").click(function(){
        location.href="./signup.html"
     })

});
