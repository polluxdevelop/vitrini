$(document).ready(function(){
	$("#buscar").validade({
		rules:
		{
			caixa_busca:
			{
				minlengh: 1
			}
		},
		messages:
		{
			caixa_busca: {minlengh: "Digite algo para buscar"}
		}
	}),
	submitHandler: function(form)
	{
		var texto_busca=$("#caixa_busca").val();
		$.ajax({
			type="POST",
			url: "/php/search.php",
			datatype: "html",
			data: "texto_busca="+texto_busca,
			success: function(data)
			{
				console.log(data);
			}
			failure: function(data)
			{
				console.log("erro");
				console.log(data);
			}
		});
	}
});