﻿$(document).ready(function()
{

    $('#cadastro').each(function(){
        this.reset();
    })
    $("#cancelar_cadastro").click(function(){
        location.href="index.html";
    });
	$("#cadastro").validate({
                rules: {
                    nome: {required:true},
                    sobrenome: {required:true},

                    usuario:{
                    	required: true,
                    	minlength: 4
                    },

                    email: {
                        required: true,
                        email: true
                    },
                    senha: {
                        required: true,
                        minlength: 8
                    },

                    senha_confirma: {
                    	required: true,
                    	minlength: 8,
                    	equalTo: "#senha"
                    },

                    site: {
                        required: false,
                        url: true
                    }



                },
                messages: {
                    nome: {required: "Por favor digite seu nome"},
                    sobrenome: {required: "Por favor digite seu sobrenome"},

                    usuario:{
                    	required: 'Por favor insira um nome de usuario',
                    	minlength: 'Nome de usuario deve conter no mínimo 4 caracteres'
                    },
                    senha: {
                        required: "Por favor digite uma senha",
                        minlength: "Sua senha deve ter no mínimo 8 caracteres"
                    },

                    senha_confirma: {
                    	required: "Por favor confirme sua senha",
                    	minlength: "Sua senha deve ter no mínimo 8 caracteres",
                    	equalTo: "Senhas não são iguais"
                    },

                    site: {
                        url:"Por favor insira uma url válida"
                    },

                    email: {
                        required: "Por favor insira o seu email",
                        email:"Por favor insira um endereço de email válido"
                    }


                },
                submitHandler: function(form) {
                    var nome = $("#nome").val();
                    var sobrenome = $("#sobrenome").val();
                    var usuario = $("#usuario").val();
                    var senha = $("#senha").val();
                    var email = $("#email").val();
                    var tel = $("#tel").val();
                    var site = $("#site").val();
                    var values = "nome="+nome+"&sobrenome="+sobrenome+
                    "&usuario="+usuario+"&senha="+senha+"&email="+email+"&telefone="+tel+"&site="+site;

                    var server = "php/";
                    var file = "signup.php";

                    $("#status").html("Enviando...");
                    $.ajax({
                        type: "POST",
                        url: server+file,
                        datatype: "html",
                        data: values,
                        success: function(data){
                            data = parseInt(data);
                            if(data>0){
                                localStorage.user = usuario;
                                localStorage.pass = senha;
                               var n = noty({
                                    text: "Cadastro efetuado! <a href='profile.html'><b>Clique para ser redirecionado</b></a>'",
                                    type: 'success',
                                    layout: 'bottom',
                                    animation: {
                                    open: {height: 'toggle'}, // jQuery animate function property object
                                    close: {height: 'toggle'}, // jQuery animate function property object
                                    easing: 'swing', // easing
                                    speed: 500, // opening & closing animation speed
                                    }
                            });
                            }
                            else
                                if(data==0)
                                {
                                   var n = noty({
                                    text: 'Nome de usuário já em uso',
                                    type: 'warning',
                                    layout: 'bottom',
                                    animation: {
                                    open: {height: 'toggle'}, // jQuery animate function property object
                                    close: {height: 'toggle'}, // jQuery animate function property object
                                    easing: 'swing', // easing
                                    speed: 500 // opening & closing animation speed
                                }
                            });
                                }
                        },
                        error: function(data)
                        {
                            var n = noty({
                                text: 'Erro de comunicação com o banco de dados!',
                                    type: 'error',
                                    layout: 'bottom',
                                    animation: {
                                    open: {height: 'toggle'}, // jQuery animate function property object
                                    close: {height: 'toggle'}, // jQuery animate function property object
                                    easing: 'swing', // easing
                                    speed: 500 // opening & closing animation speed
                                }
                            });
                        }

                    });

                    return false;

                }
            });

});