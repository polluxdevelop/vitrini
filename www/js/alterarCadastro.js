$(document).ready(function()
{
   $("#alterar_cadastro").validate({
       rules: {
                    nome: {required:false},
                    sobrenome: {required:false},

                    usuario:{
                    	required: false,
                    	minlength: 4
                    },

                    email: {
                        required: false,
                        email: false
                    },
                    senha: {
                        required: false,
                        minlength: 8
                    },

                    senha_confirma: {
                    	required: false,
                    	minlength: 8,
                    	equalTo: "#senha"
                    }



                },
                messages: {
                    sobrenome: {required: "Por favor digite seu sobrenome"},

                    usuario:{
                    	minlength: 'Nome de usuario deve conter no mínimo 4 caracteres'
                    },
                    senha: {
                        minlength: "Sua senha deve ter no mínimo 8 caracteres"
                    },

                    senha_confirma: {
                    	minlength: "Sua senha deve ter no mínimo 8 caracteres",
                    	equalTo: "Senhas não são iguais"
                    },

                    email: "Por favor insira um endereço de email válido"


                },
                submitHandler: function(form) {
                    var values;
                    if($("#nome").val()!="")
                    {
                        values+="&nome="+$("#nome").val();
                    }
                    if($("#sobrenome").val()!="")
                    {
                        values+="&sobrenome"+$("#sobrenome").val();
                    }
                    if($("#usuario").val()!="")
                    {
                        values+="&usuario="+localStorage.user;
                    }
                    else
                        values+="&usuario="+$("#usuario").val();
                    if($("#email").val()!="")
                    {
                        values+="&email="+$("#email").val();
                    }
                    if($("#senha").val()!="")
                    {
                        values+="&pass="+localStorage.pass;
                    }
                    else
                        values+="&pass="+$("#senha").val();
                    if($("#telefone").val()!="")
                    {
                        values+="&telefone"+$("#telefone").val();
                    }
                    var server = "php/";
                    var file = "update.php";

                    $("#status").html("Enviando...");
                    $.ajax({
                        type: "POST",
                        url: server+file,
                        datatype: "html",
                        data: values,
                        success: function(data){
                            noty({
                                text: "Campos alterados com sucesso",
                                layout: "bottom",
                                type: "success"
                            });
                        }

                    });

                    return false;

                }
            });
   });
