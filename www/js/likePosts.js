function likePost(post){
        if(validateSession()){
            var values = "usuario="+localStorage.user+"&pass="+localStorage.pass+"&idPost="+post+"&code=2";

            $.ajax({
                type: "POST",
                url: server+"posts.php",
                datatype: "html",
                data: values,
                success: function(data){
                    // console.log(data);
                    // $("#status").html(data);
                    var n = noty({
                        text: 'Você curtiu um post',
                        layout: 'topRight',
                        type: 'information'
                    });
                },
                error: function(data)
                {
                    noty({
                        text: 'Erro de conexão com o banco de dados',
                        layout: 'bottom',
                        type: 'error'
                    });
                }

            });

        }
    }

function addInterest(post){
        if(validateSession()){
            var values = "usuario="+localStorage.user+"&pass="+localStorage.pass+"&interest="+post;

            $.ajax({
                type: "POST",
                url: server+"update.php",
                datatype: "html",
                data: values,
                success: function(data){
                    console.log(data);
                    $("#status").html(data);
                }

            });

        }
    }

$(document).ready(function(){
    $(".btn-favoritar img").click(function(){
        var n = window.location.search;
        n = n.substring(3);
        likePost(n);

    });

    $(".btn-selar img").click(function(){
        var n = window.location.search;
        n = n.substring(3);
        addInterest(n);

    });
});