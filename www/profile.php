<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Pollux Social - Perfil</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        

        <script type="text/javascript" src='js/lib/jquery-2.2.0.min.js'></script>
        <script type="text/javascript" src="js/lib/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/dropdown.js"></script> 

        <!--<script type="text/javascript" src="js/profile.js"></script>-->
         <script type="text/javascript" src="js/abas.js" ></script> 
         <script type="text/javascript" src="js/selar.js" ></script> 
         
    <link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/3.0.3/normalize.css">
        <link rel='stylesheet' type='text/css' href='css/profile.css'/>
        
         <link rel="stylesheet" type="text/css" href="css/drop1.css"/>
         <link rel="stylesheet" type="text/css" href="css/abas.css"/>
         <link rel="stylesheet" type="text/css" href="css/definirFonte.css"/>
         <link rel="stylesheet" type="text/css" href="css/users.css"/>
    </head>
    <body>
  <nav class="nav-bar-perf">            
               

                <div id="nav-bar-titulo"><a class="a_titulo" href="intro.html">Pollux</a></div>
                       <div class="form-group">
                          <form id="buscar">
                                <input class="form-control form-control-pollux" type="text" placeholder="Pesquisar" id="caixa_busca">
                                 <span id="categoria_span">
                                    <a id="categoria_img" href="#"><img id="categoria_img1" src="img/categoria.png"></a>
                           </span>                           
                       </form>
                  </div>
                        
    
                  
                       <div class="icones">
                       <div id="img_usuario">
                        <div class="bs-example-navbar-collapse-1 img-usuario dropdown">
                         <ul class="nav nav-pills">
                            <li class="img-usuario">
                                <div id="icone_profile">
                                       <a href="profile.html"><img src='img/usuario.png' class="imagem-menu" alt="foto do usuário"/></a>
                                       <span id="nome_usuario"></span>                             
                              </div>
                           </li>           
                        </ul>
                     </div>
                     </div>
                     <div id="luz"><a href="#"><img class="img_drop" src="img/luz.png"></a> </div>
                     <div id="sino"><a href="#"><img class="img_drop" src="img/sino.png"></a></div>
                    <div id="lapis"><a href="create.html"><img class="img_drop" src="img/lapis.png"></a></div>
                    <div class="img-menu">
                       
                       
                        <a href="#"><img id="img_drop" src="img/roda.png"></a>

                             <div id="drop_config" class="dropdown-menu">
                                <div class="dropdown-caret">
                                  <span class="caret-outer"></span>
                                  <span class="caret-inner"></span>
                                </div> 
                             <ul>        
                                    <li><a href="alterarPerfil.html">Alterar Perfil</a></li>
                                    <li><a id="sair">Sair</a></li>
                             </ul>                           
                             </div>
                </div>
               </div> 
   
  </nav>

     <div id="dropdown-categorias">
                  <div id="divisao">
                  <span id="categoria_titulo">Áreas de Interesse</span>                
                   <div id="div1">     
                                     <a>FOTOGRAFIA</a><br>
                                     <a>ILUSTRAÇÃO</a><br>
                                     <a>PROGRAMAÇÃO</a><br>
                    </div>
                    <div id="div2">
                                     <a>DESIGN</a><br>
                                     <a>DESIGN DE SOM</a><br>
                                     <a>NARRATIVAS</a><br>
                     </div>
                      <div id="div3">
                                     <a>ANIMAÇÃO</a><br>
                                     <a>AUDIO VISUAL</a><br>
                      </div>
                 </div>               
                              
   </div>
<div id="conteudo">
        
        <!--

        <div id='profile'>
        
          <div id="profile-photo-container">
           <img src="http://placehold.it/150x150" alt="Foto de perfil do usuário" id="profile-photo" width="150px" />
          </div>
      
         <div class="dados">
          <span id="complete-name"></span>
          <span id="ocupation"></span>
          <span id="site-container">
            <a href="" target="_blank" id="site"></a>
          </span>  
          <div class='icons'>
             <section>
               <span  id="visualizado"><a><img src="img/coment1.png" alt=""></a></span>
               <span  id="comentado"><a><img src="img/coment2.png" alt=""></a></span>
             </section>
            <section>
                   <span  id="favorito"><a><img src="img/curtida.png" alt=""></a></span>
                   
                   <span  id="envolvidos"><a><img src="img/coment3.png" alt=""></a></span>  
            </section>
            
          </div>
         </div> 
           <section id="titulo_area">Áreas de Interesse</section>
         <div id="profile-areas">
            
           </div>
        </div>
      <div class="tabs">
        <ul class="tab-links">
          <li id="menu1"class="active"><a id="tab1">PUBLICAÇÕES</a></li>
          <li id="menu2"><a id="tab2">INSPIRAÇÕES</a></li>
         </ul>
          
    
        <div id="p_content" class="tab active">
            <article id="users"></article><br />

            <article id="posts">
                 <div id="personal-posts"></div><br />



            </article>
            <span id="status"></span>
        </div>
      <div id="inspirar" class="tab">
        
      </div>
      </div>

       </div> 
     -->

     <?php
     
     
          if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET)){
     
            
           $user = $_GET["n"];
     
     
           $mysqli = new mysqli('localhost', 'root', '', 'pollux');
           mysqli_set_charset($mysqli, 'utf8');
     
           if($mysqli){
     
                        function convert_array_to_reference($arr){
                          $refs = array();
                          foreach($arr as $key => $value)
                            $refs[$key] = &$arr[$key];
                          return $refs;

                        }


                        $stmt = $mysqli->prepare("SELECT id,nome,sobrenome,areas,inspiracoes,site,ocupacao,telefone,email,foto,nProjetos,nPosts,nSeguidores,nLikes,nVisualizacoes FROM usuarios WHERE usuario = ? LIMIT 1");
                        $stmt->bind_param("s", $user);

                        if($stmt->execute()){

                          $result = $stmt->get_result();

                          while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                          {
                            $ret = (object)$row;
                          }

                          $stmt->close();


                          if($ret->areas != null){
                          $sql = "SELECT * FROM areas";
                          $stmt = $mysqli->prepare($sql);

                            if($stmt->execute()){



                              $areas = array();

                              $r = $stmt->get_result();

                              while($ro = $r->fetch_array(MYSQLI_ASSOC)) 
                              {
                                $areas[] = (object)$ro;
                              }  

                              $profAreas = explode("/", $ret->areas,-1);

                              $pareas = "";
                              foreach ($profAreas as $area) {
                                $pareas .= '<div class="area"><span>'.$areas[$area-1]->nome.'</span></div>';
                              }

                              #echo json_encode($profAreas);

                              $select = "(SELECT id FROM usuarios WHERE usuario=? LIMIT 1)";

                              $sql = "SELECT posts.id as 'id', posts.nComentarios as 'ncomment', posts.nome as 'titulo', posts.conteudo as 'conteudo',";
                              $sql .= "usuarios.usuario as 'usuario', posts.areas, posts.criado as 'criado', projetos.nome as 'projectname'";
                              $sql .= " FROM posts LEFT JOIN projetos ON posts.id_projeto = projetos.id LEFT JOIN usuarios ON ";
                              $sql .= "posts.id_usuario=usuarios.id WHERE posts.id_usuario =".$select." ORDER BY posts.criado DESC";

                              $stmt = $mysqli->prepare($sql);
                              $stmt->bind_param("s", $user);

                              if($stmt->execute()){

                                $result = $stmt->get_result();

                                $posts = array();

                                while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                                {
                                  $posts[] = (object)$row;
                                }

                                $postshtml="";

                                foreach ($posts as $post) {
                                  $postarea = explode("/", $post->areas,-1);

                                  $postareahtml = '';
                                  foreach ($postarea as $poArea) {
                                    $postareahtml .= '<div id="area"><span>'.$areas[$poArea-1]->nome.',</span></div>';
                                  }

                                  $postshtml .= '<div class="fllw-post">'.
                                        '<div class="fllw-post-arrow"></div>'.
                                            '<h3 class="fllw-post-title">'.$post->titulo.'</h3>'.
                                            '<h5 class="fllw-post-author">por <a href="#">'.$post->usuario.'</a></h5>'.
                                            '<hr>'.
                                            '<h5 class="fllw-post-filter">Filtro: '.$postareahtml.'</h5>'.
                                            '<div class="fllw-post-img"></div>'.
                                            '<div class="fllw-post-bar">'.
                                            '<div class="bar-info">'.
                                            '<span class="info-views"><img src="img/coment1.png"></span>'.
                                            '<span class="info-comments"><img src="img/coments.png">'.$post->ncomment.'</span>'.
                                            '</div>'.
                                            '<div class="bar-buttons">'.

                                               '<span class="btn-selar"><img src="img/notif_des.png"></span>'.
                                               '<span class="btn-favoritar"><img src="img/like_des.png"></span>'.
                                        '</div>'.
                                        '</div>'.
                                    '</div>';

                                }
                              }



                              $completename = $ret->nome[strlen($ret->nome)-1] == " " || $ret->sobrenome[0] == " " ? $ret->nome.$ret->sobrenome : $ret->nome." ".$ret->sobrenome;

                                echo '<div id="profile">'.
                                    '<div id="profile-photo-container">'.
                                    '<img src="'.$ret->foto.'" alt="Foto de perfil do usuário" id="profile-photo" width="150px" />'.
                                    '</div>'.
                                    '<div class="dados">'.
                                    '<span id="complete-name">'.$completename.'</span>'.
                                    '<span id="ocupation">'.$ret->ocupacao.'</span>'.
                                    '<span id="site-container">'.
                                    '<a href="'.$ret->site.'" target="_blank" id="site">'.$ret->site.'</a>'.
                                    '</span>'.
                                    '<div class="icons">'.
                                    '<section>'.
                                    '<span  id="visualizado"><a><img src="img/coment1.png" alt="">'.$ret->nVisualizacoes.'</a></span>'.
                                    '<span  id="comentado"><a><img src="img/coment2.png" alt="">'.$ret->nPosts.'</a></span>'.
                                    '</section>'.
                                    '<section>'.
                                    '<span  id="favorito"><a><img src="img/curtida.png" alt="">'.$ret->nLikes.'</a></span>'.
                                    '<span  id="envolvidos"><a><img src="img/coment3.png" alt="">'.$ret->nSeguidores.'</a></span>'. 
                                    '</section>'.
                                    '</div>'.
                                    '</div>'.
                                    '<section id="titulo_area">Áreas de Interesse</section>'.
                                    '<div id="profile-areas">'.
                                    $pareas.
                                    '</div>'.
                                    '</div>'.
                                    '<div class="tabs">'.
                                    '<ul class="tab-links">'.
                                    '<li id="menu1"class="active"><a id="tab1">PUBLICAÇÕES</a></li>'.
                                    '<li id="menu2"><a id="tab2">INSPIRAÇÕES</a></li>'.
                                    '</ul>'.
                                    '<div id="p_content" class="tab active">'.
                                    '<article id="users"></article><br />'.
                                    '<article id="posts">'.
                                    '<div id="personal-posts">'.$postshtml.'</div><br />'.
                                    '</article>'.
                                    '<span id="status"></span>'.
                                    '</div>'.
                                    '<div id="inspirar" class="tab">'.
                                    '</div>'.
                                    '</div>';


                              $stmt->close();
                              $mysqli->close();

                            }else{

                              echo $stmt->error;

                                                #echo 0;
                              $stmt->close();
                              $mysqli->close();
                            }

            }else{
              #echo json_encode($ret);

              #echo $ret->foto;

              $mysqli->close();
            }
            }else{
              echo 0;

              $stmt->close();
              $mysqli->close();
            }

           }else{
             echo 0;
           }
     
         }else{
           echo 0;
         }
     
     
         ?>
     
    </body>
</html>
