<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Pollux Social - Perfil</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        

        <script type="text/javascript" src='js/lib/jquery-2.2.0.min.js'></script>
        <script src="js/likePosts.js"></script>
        <link rel='stylesheet' type='text/css' href='css/profile.css'/>
        
    </head>
    <body>
         <nav class="nav-bar-perf">            
               

                <div id="nav-bar-titulo"><a class="a_titulo" href="intro.html">Pollux</a></div>
                       <div class="form-group">
                          <form id="buscar">
                                <input class="form-control form-control-pollux" type="text" placeholder="Pesquisar" id="caixa_busca">
                                 <span id="categoria_span">
                                    <a id="categoria_img" href="#"><img id="categoria_img1" src="img/categoria.png"></a>
                           </span>                           
                       </form>
                  </div>
                        
    
                  
                       <div class="icones">
                       <div id="img_usuario">
                        <div class="bs-example-navbar-collapse-1 img-usuario dropdown">
                         <ul class="nav nav-pills">
                            <li class="img-usuario">
                                <div id="icone_profile">
                                       <a href="profile.html"><img src='img/usuario.png' class="imagem-menu" alt="foto do usuário"/></a>
                                       <span id="nome_usuario"></span>                             
                              </div>
                           </li>           
                        </ul>
                     </div>
                     </div>
                     <div id="luz"><a href="#"><img class="img_drop" src="img/luz.png"></a> </div>
                     <div id="sino"><a href="#"><img class="img_drop" src="img/sino.png"></a></div>
                    <div id="lapis"><a href="create.html"><img class="img_drop" src="img/lapis.png"></a></div>
                    <div class="img-menu">
                       
                       
                        <a href="#"><img id="img_drop" src="img/roda.png"></a>

                             <div id="drop_config" class="dropdown-menu">
                                <div class="dropdown-caret">
                                  <span class="caret-outer"></span>
                                  <span class="caret-inner"></span>
                                </div> 
                             <ul>        
                                    <li><a href="alterarPerfil.html">Alterar Perfil</a></li>
                                    <li><a id="sair">Sair</a></li>
                             </ul>                           
                             </div>
                </div>
               </div> 
   
  </nav>


        <header>
        <ul id="dropdown">
          <li><a href="alterarPerfil.html">Alterar Perfil</a></li>
          <li><a href="#" id="sair">Sair</a></li>
        </ul>
        </header>


<div id="conteudo">
        <section>
        
          <div id="profile-photo-container">
            <img src="http://placehold.it/150x150" alt="Foto de perfil do usuário" id="profile-photo" width="150px" />
          </div>
      
          <span id="complete-name"></span>
          <span id="ocupation"></span>

          <span id="site-container">
            <a href="" target="_blank" id="site"></a>
          </span>  

          <div id="profile-areas">
            
          </div>
            

        </section>
    

        <section>
            

            <article id="posts">
                 

            </article>

            <?php


            if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET)){


              $id=$_GET['n'];


              $mysqli = new mysqli('localhost', 'root', '', 'pollux');

              if($mysqli){

                $sql = "SELECT nome,nome_grupo,areas,autores,capa,descricao,id_usuario,nLikes,nPosts,criado ";
                $sql .= "FROM projetos WHERE id = ?";

                $stmt = $mysqli->prepare($sql);
                $stmt->bind_param("i",$id);

                if($stmt->execute()){

                  $arr = array();

                  $result = $stmt->get_result();

                  while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                  {
                      $arr[] = $row;
                  }

                  $proj = (object)$arr[0];

                  $stmt->close();

                  $sql = "SELECT posts.id as 'id', posts.nComentarios as 'ncomment', posts.nome as 'titulo', posts.conteudo as 'conteudo',";
                  $sql .= "usuarios.usuario as 'usuario', posts.nLikes as 'nLikes', posts.nInspirados, posts.criado as 'criado'";
                  $sql .= " FROM posts LEFT JOIN projetos ON posts.id_projeto = projetos.id LEFT JOIN usuarios ON ";
                  $sql .= "posts.id_usuario=usuarios.id WHERE posts.id_projeto = ? ORDER BY posts.criado DESC";

                  $stmt = $mysqli->prepare($sql);
                  $stmt->bind_param("i", $id);

                 if($stmt->execute()){
                  $arr2 = array();

                  $result = $stmt->get_result();

                  while($row = $result->fetch_array(MYSQLI_ASSOC)) 
                  {
                      $arr2[] = (object)$row;
                  }

                  echo '<article id="following-posts">'.
                      '<div class="fllw-post post-fullscreen">'.
                      '<h2 class="fllw-post-title">'.$proj->nome.'</h2>'.
                      '<h5 class="fllw-post-author">por <a href="#">'.$proj->nome_grupo.'</a></h5>'.
                      '<div class="fllw-post-img"><img src="'.$proj->capa.'"/></div>'.
                      '</div>';


                  $sql = "SELECT * FROM areas";
                  $stmt = $mysqli->prepare($sql);

                            if($stmt->execute()){
                              $areas = array();

                              $r = $stmt->get_result();

                              while($ro = $r->fetch_array(MYSQLI_ASSOC)) 
                              {
                                $areas[] = (object)$ro;
                              }  

                              }


                  if(count($arr2)>0){

                    foreach ($arr2 as $post) {

                      $postAreas = explode("/", $post->areas,-1);

                              $pareas = "";
                              foreach ($postAreas as $area) {
                                $pareas .= '<div class="area"><span>'.$areas[$area-1]->nome.'</span></div>';
                              }

                      $postshtml .= '<div class="fllw-post">'.
                                        '<div class="fllw-post-arrow"></div>'.
                                            '<h3 class="fllw-post-title">'.$post->titulo.'</h3>'.
                                            '<h5 class="fllw-post-author">por <a href="#">'.$post->usuario.'</a></h5>'.
                                            '<hr>'.
                                            '<h5 class="fllw-post-filter">Filtro: '.$pareas.'</h5>'.
                                            '<div class="fllw-post-img"></div>'.
                                            '<div class="fllw-post-bar">'.
                                            '<div class="bar-info">'.
                                            '<span class="info-views"><img src="img/coment1.png"></span>'.
                                            '<span class="info-comments"><img src="img/coments.png">'.$post->ncomment.'</span>'.
                                            '</div>'.
                                            '<div class="bar-buttons">'.
                                            '<span class="btn-selar"><img id="selar" src="img/notif_des.png"></span>'.
                                            '<span class="btn-favoritar"><img id="favoritar" src="img/like_des.png"></span>'.
                                        '</div>'.
                                    '</div>';
                    }
                    


                  }else{
                    echo "<span class='no-post'>Esse projeto não possui posts.</span>";
                  }

                  $stmt->close();
                  $mysqli->close();

                  }else{
                      echo 0;

                      $stmt->close();
                      $mysqli->close();
                  }


                }else{
                      echo 0;

                      $stmt->close();
                      $mysqli->close();
                  }


                

              }else{
                echo 0;
              }

            }else{
              echo 0;
            }


            ?>

            <span id="status"></span>
        </section>
      </div>
    </body>
</html>
